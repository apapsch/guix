(define-module (gnu packages proxysql)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gnunet)
  #:use-module (gnu packages python)
  #:use-module (gnu packages tls)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public clickhouse-cpp
  (package
    (name "clickhouse-cpp")
    (version "0.0.0-1.7c67c6d")
    (home-page "https://github.com/ClickHouse/clickhouse-cpp")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url home-page)
                    (commit "7c67c6dfb91b9f5afcae218ed274187cb0292039")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0glwq7nsjiqb08sas6ryvr5qb6fsfpg42sj91hps33ncvfnjkrvk"))))
    (build-system cmake-build-system)
    (arguments '(#:tests? #f))
    (synopsis "Client library for ClickHouse")
    (description
     "C++ client library for ClickHouse, a column-oriented DBMS
for online analytical processing.")
    (license license:asl2.0)))

(define-public libhttpserver
  (package
    (name "libhttpserver")
    (version "0.17.5-1.e0fd7a2")
    (home-page "https://github.com/etr/libhttpserver")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url home-page)
                    (commit "e0fd7a27568caf82fad9cbe2e4dd3ce7a7532fc0")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ikmhn1lkb9fv74wcik5xlw3vwyczr29m3zys9mm1jxbh8jq9rp8"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags '("--enable-same-directory-build")
       #:tests? #f))
    (inputs
     `(("curl" ,curl)
       ("gnutls" ,gnutls)
       ("libmicrohttpd" ,libmicrohttpd)))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("libtool" ,libtool)))
    (synopsis "C++ library for creating an embedded REST HTTP server")
    (description "FIXME")
    (license license:lgpl2.1)))

(define-public libinjection
  (package
    (name "libinjection")
    (version "3.10.0")
    (home-page "https://github.com/client9/libinjection")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url home-page)
                    (commit "bf234eb2f385b969c4f803b35fda53cffdd93922")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0chsgam5dqr9vjfhdcp8cgk7la6nf3lq44zs6z6si98cq743550g"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda* (#:key outputs #:allow-other-keys)
             (make-file-writable "src/sqlparse_data.json")
             (make-file-writable "src/libinjection_sqli_data.h")
             (substitute* "src/Makefile"
               (("/usr/local") (assoc-ref outputs "out"))
               (("CC=cc")      "CC=gcc"))
             #t))
         (add-before 'install 'go-to-correct-makefile
           (lambda _
             (chdir "src"))))))
    (native-inputs
     `(("python" ,python-2)))
    (synopsis "SQL / SQLI tokenizer parser analyzer")
    (description "FIXME")
    (license license:bsd-3)))
